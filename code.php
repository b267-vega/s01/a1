<?php

function getFullAddress($blk,$city,$province,$country){
    return "$blk,$city,$province,$country";
}

function getLetterGrade($num){
    if ($num > 98 && $num <= 98){return 'A+';}
    elseif($num > 95 && $num <= 97){return 'A';}
    elseif($num > 92 && $num <= 94){return 'A-';}
    elseif($num > 89 && $num <= 91){return 'B+';}
    elseif($num > 86 && $num <= 88){return 'B';}
    elseif($num > 83 && $num <= 85){return 'B-';}
    elseif($num > 80 && $num <= 82){return 'C+';}
    elseif($num > 77 && $num <= 79){return 'C';}
    elseif($num > 75 && $num <= 76){return 'C-';}
    else{return 'D';}
}